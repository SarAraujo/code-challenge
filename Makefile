
SRC_PATH=src
BIN_PATH=bin
CC=g++ -std=c++14

ifndef RESTBED_MODULES_PATH
export RESTBED_MODULES_PATH=/restbed
endif

INCLUDES = -I $(RESTBED_MODULES_PATH)/distribution/include/ -I $(SRC_PATH)
LIBRARIES = -L $(RESTBED_MODULES_PATH)/distribution/library -lrestbed

LDFLAGS=$(INCLUDES)
LDLIBS=$(LIBRARIES)
CPPFLAGS=-g -Wall $(INCLUDES)

all:: docker

docker::
	docker build -t interview-testing:latest .

release::
	mkdir -p $(BIN_PATH)
	$(CC) $(CPPFLAGS) $(SRC_PATH)/*.cpp -o $(BIN_PATH)/$@ $(LDLIBS)

.PHONY:: clean
clean::
	rm -rf $(BIN_PATH)

