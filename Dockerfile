FROM alpine:3.6


RUN apk add --no-cache -q -f git cmake make g++
RUN apk add --no-cache make gcc gawk bison linux-headers libc-dev
RUN git clone --recursive https://github.com/corvusoft/restbed.git \
  && mkdir -p /restbed/build \
  && cd /restbed/build \
  && cmake -DBUILD_SSL=NO -DBUILD_TESTS=NO .. \
  && make -j4 install

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/restbed/distribution/library

WORKDIR /tmp/cpp/
COPY . .
RUN make release -j4

ENTRYPOINT ["/tmp/cpp/bin/release"]
CMD /tmp/cpp/bin/release
