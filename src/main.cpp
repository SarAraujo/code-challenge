#include <iostream>
#include <string.h>
#include <moran.h>
#include <restbed>


using namespace std;
using namespace restbed;

void get_float_handler( const shared_ptr< Session > session )
{
    const auto& request = session->get_request( );
    HarshadMoran mh;
    float value = std::stof(request->get_path_parameter( "number" ));
    const string body = mh.getType(value);
    session->close( OK, body, { { "Content-Length", ::to_string( body.size( ) ) } } );
}

void get_i32_handler( const shared_ptr< Session > session )
{
    const auto& request = session->get_request( );
    HarshadMoran mh;
    int32_t value = std::stoi(request->get_path_parameter( "number" ));
    const string body = mh.getType(value);
    session->close( OK, body, { { "Content-Length", ::to_string( body.size( ) ) } } );
}

void get_u16_handler( const shared_ptr< Session > session )
{
    const auto& request = session->get_request( );
    HarshadMoran mh;
    uint16_t value = std::stoul(request->get_path_parameter( "number" ));
    const string body = mh.getType(value);
    session->close( OK, body, { { "Content-Length", ::to_string( body.size( ) ) } } );
}

void get_string_handler( const shared_ptr< Session > session )
{
    const auto& request = session->get_request( );
    HarshadMoran mh;
    string value = request->get_path_parameter( "number" );
    const string body = mh.getType(value);
    session->close( OK, body, { { "Content-Length", ::to_string( body.size( ) ) } } );
}

void service_ready_handler( Service& )
{
    fprintf( stderr, "The service is up and running.\n" );
}

int main( const int, const char** )
{
    auto float_numbers = make_shared< Resource >( );
    float_numbers->set_path( "/moran/float/{number: .*}");
    float_numbers->set_method_handler("GET", get_float_handler);

    auto integer32 = make_shared< Resource >( );
    integer32->set_path( "/moran/i32/{number: .*}" );
    integer32->set_method_handler("GET", get_i32_handler);

    auto unsigned16 = make_shared< Resource >( );
    unsigned16->set_path( "/moran/u16/{number: .*}" );
    unsigned16->set_method_handler("GET", get_u16_handler);

    auto str = make_shared< Resource >( );
    str->set_path( "/moran/string/{number: .*}" );
    str->set_method_handler("GET", get_string_handler);

    auto settings = make_shared< Settings >( );
    settings->set_port( 80 );
    settings->set_default_header("Connection", "close");

    auto service = make_shared< Service >( );
    service->publish(float_numbers);
    service->publish(integer32);
    service->publish(unsigned16);
    service->publish(str);
    service->set_ready_handler( service_ready_handler );
    service->start(settings);

    return EXIT_SUCCESS;
}
