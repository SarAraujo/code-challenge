#include <string>
#include <math.h>

using namespace std;

class HarshadMoran {
public:

    template <typename DataType>
    std::string getType(DataType input) {
        return is_harshad((int32_t)round(input));
    }

    std::string getType(std::string input) {
        int32_t value = std::stof(input);
        return is_harshad(value);
}

private:
    std::string is_harshad(int32_t input);
    
    bool is_prime(int32_t  number);
};