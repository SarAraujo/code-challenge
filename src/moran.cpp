#include <moran.h>


std::string HarshadMoran::is_harshad(int32_t input) {
    uint32_t aux_input = input;
    uint32_t sum = 0;

    // sum each digit
    while(aux_input > 0) {
        sum += aux_input % 10;
        aux_input = aux_input / 10;
    }

    // if the value is divisible by sum of its digits
    // the rest shall be zero
    if((0 < input) && input % sum == 0) {
        if(is_prime(input / sum)) {
            return "M";
        }
        else {
            return "H";
        }           
    }
    else {
        return "N";
    }
}

bool HarshadMoran::is_prime(int32_t number) {
    for (int32_t i = 2; i*i < number; i++)
        if (number % i == 0)
            return false;

    return true;
}